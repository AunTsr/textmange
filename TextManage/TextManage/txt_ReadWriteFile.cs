﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace TextManage
{
    public class txt_ReadWriteFile
    {
        private static string LocalPath = AppDomain.CurrentDomain.BaseDirectory.ToString();

        public static List<string> getFilenameWithoutExtension(string filepath, string filter)
        {
            var list = Directory.GetFiles(filepath, filter);
            return list.Select(file => Path.GetFileNameWithoutExtension(file)).ToList();
        }

        #region For Text
        public static string ReadTxtfile(string filepath)
        {
            try
            {
                return File.ReadAllText(LocalPath + filepath);
            }
            catch
            {
                return "";
            }
        }
        public static bool SaveTxt2file(string filepath, string txt)
        {
            try
            {
                File.WriteAllText(LocalPath + filepath, txt);
                return true;
            }
            catch
            {

                return false;
            }
        }
        public static bool AppendTxt2file(string filepath, string txt)
        {
            try
            {
                File.AppendAllText(LocalPath + filepath, txt);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        public static string ObjtoXml<T>(T obj)
        {
            try
            {
                XmlSerializer xsSubmit = new XmlSerializer(obj.GetType());
                Type type = obj.GetType();
                var xmlsetting = new XmlWriterSettings();
                //xmlsetting.OmitXmlDeclaration=true;
                //xmlsetting.ConformanceLevel = ConformanceLevel.Fragment;

                using (var sww = new StringWriter())
                {
                    using (XmlWriter writer = XmlWriter.Create(sww, xmlsetting))
                    {
                        xsSubmit.Serialize(writer, obj);
                        return sww.ToString();
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public static List<T> XmltoObj<T>(string xml)
        {
            try
            {
                //XmlSerializer serializer = new XmlSerializer(typeof(T));
                //StringReader rdr = new StringReader(xml);

                var tArr = Activator.CreateInstance<List<T>>();
                var xmldoc = new XmlDocument();
                xmldoc.LoadXml(xml);
                XmlNodeList xmlnodelst = xmldoc.GetElementsByTagName(typeof(T).Name);
                foreach (XmlNode nod in xmlnodelst)
                {
                    //XmlSerializer serializer = new XmlSerializer(typeof(T));
                    //StringReader rdr = new StringReader(nod.InnerXml);
                    //var result = (T)serializer.Deserialize(rdr);

                    var t = Activator.CreateInstance<T>();
                    mapAtt2props(ref t, nod);
                    //foreach (XmlNode ele in nod.ChildNodes)
                    //{
                    //    t.GetType().GetProperty(ele.Name).SetValue(t, ele.Value);
                    //}

                    tArr.Add(t);
                }

                return default(List<T>);
            }
            catch (Exception ex)
            {
                return default(List<T>);
            }
        }

        private static void mapAtt2props<T>(ref T obj, XmlNode node)
        {
            XDocument doc = XDocument.Parse(node.InnerXml); //or XDocument.Load(path)
            string jsonText = JsonConvert.SerializeXNode(doc);
            dynamic dyn = JsonConvert.DeserializeObject<T>(jsonText);

            //if (node.ChildNodes.Count == 1) return;
            //foreach (XmlElement nod in node)
            //{
            //    mapAtt2props(ref obj, nod);
            //    if(nod.InnerXml!="") obj.GetType().GetProperty(nod.Name).SetValue(obj, nod.Value);
            //}
        }

        #region JsonObj
        public static T JsontoObj<T>(string json)
        {
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //T obj = JsonConvert.DeserializeObject<T>(json);

            return JsonConvert.DeserializeObject<T>(json);
        }

        public static string ObjToJson(object obj)
        {
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //var output = JsonConvert.SerializeObject(obj);
            return JsonConvert.SerializeObject(obj);
        }
        #endregion

    }
}
