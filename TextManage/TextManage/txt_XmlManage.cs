﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TextManage
{
    public class txt_XmlManage
    {
        public string PathFile { get; set; }
        public int range { get; set; }
        public string PathFileBK { get; set; }
        public string[] fileNames;
        public string TagName = "";

        public Dictionary<string, object> xmldata = new Dictionary<string, object>();
        public List<Dictionary<string, object>> listxmldata = new List<Dictionary<string, object>>();

        public txt_XmlManage(string PathFile, int range)
        {
            this.PathFile = PathFile;
            this.range = range;
        }

        public List<Dictionary<string, object>> GetData()
        {
            fileNames = Directory.GetFiles(PathFile, "*.XML");

            //read all file 
            foreach (string file in fileNames)
            {
                // Read a document  
                XmlTextReader textReader = new XmlTextReader(file);
                try
                {
                    //XmlTextReader textReader = new XmlTextReader(fileNames[0]);

                    int i = 0;
                    int j = 0;

                    // Read until end of file  
                    while (textReader.Read())
                    {
                        XmlNodeType nType = textReader.NodeType;

                        if (textReader.Value.ToString() != "")
                        {
                            if (i == 1)
                            {
                                try
                                {
                                    xmldata.Add(TagName, textReader.Value.ToString());
                                    i = 0;
                                    j++;
                                    TagName = "";
                                }
                                catch
                                {

                                }
                            }
                        }

                        // if node type is an element  
                        if (nType == XmlNodeType.Element)
                        {
                            if (textReader.Name.ToString() == textReader.Name.ToString())
                            {
                                i = 1;
                                TagName = textReader.Name.ToString();
                            }
                        }

                        if (j == range)
                        {
                            listxmldata.Add(xmldata);
                            j = 0;
                            xmldata = new Dictionary<string, object>();
                        }
                    }
                    textReader.Close();

                    string filename = Path.GetFileName(file);
                    var fullpath = PathFileBK + "\\" + filename;

                    if (File.Exists(fullpath))
                    {
                        File.Delete(fullpath);
                    }
                    File.Move(file, fullpath);
                }
                catch
                {

                }
            }
            return listxmldata;
        }
//======================================================================================================================================================
    
        public void AppendFile(string FilePath)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(FilePath);

//------------------------------------------------------------------------------------------------------------------------------------------------------
//          To append file from fiie path
//------------------------------------------------------------------------------------------------------------------------------------------------------
            XmlNode Done = xmlDoc.CreateNode(XmlNodeType.Element, "Done", null);
            Done.InnerText = "True";
            xmlDoc.DocumentElement.AppendChild(Done);
//------------------------------------------------------------------------------------------------------------------------------------------------------
       
            xmlDoc.Save(FilePath);
        }

        public void GenXmlFile(string Name)
        {
            using (XmlWriter writer = XmlWriter.Create(Name))
            {
                writer.WriteStartElement("OrderXML1");
                //writer.WriteStartElement("books");
                writer.WriteElementString("OrderID", "1140452");
                writer.WriteElementString("MaterialID", "1012");
                writer.WriteElementString("Weight", "100");

                writer.WriteElementString("OrderID", "1140453");
                writer.WriteElementString("MaterialID", "1012");
                writer.WriteElementString("Weight", "101");

                writer.WriteElementString("OrderID", "1140454");
                writer.WriteElementString("MaterialID", "1012");
                writer.WriteElementString("Weight", "102");

                writer.WriteElementString("OrderID", "1140455");
                writer.WriteElementString("MaterialID", "1012");
                writer.WriteElementString("Weight", "103");
                //writer.WriteElementString("price", "64.95");
                //writer.WriteEndElement();
                writer.WriteEndElement();
                writer.Flush();
            }
        }
    }
}
