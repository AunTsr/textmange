﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;
using Microsoft.VisualBasic;
using System.Windows.Forms;

namespace TextManage
{
    public static class fnReadWriteExcel
    {
        public static string paths = "";
        public static string result = "";
        public static _Excel.Application excel;
        public static Workbook wb;
        public static Worksheet ws;
        public static Range rang;

        public static string ReadAllExcel(string path, int page)
        {
            paths = path;
            ReadExcel(path, page); // create instance of Excel
            for (int i = 1; i <= rang.Rows.Count; i++)
            {
                for (int j = 1; j <= rang.Columns.Count; j++)
                {
                    if (rang.Cells[i, j] != null && rang.Cells[i, j].Value2 != null)
                        result = result + rang.Cells[i, j].Value2.ToString() + " ";
                }
                result = result + "\r\n";
            }
            ClearUp();
            return result;
        }

        public static string ReadRowColExcel(int row, int col, string path, int page)
        {
            paths = path;
            ReadExcel(path, page);
            row++;
            col++;
            if (rang.Cells[row, col].Value2 != null)
            {
                result = rang.Cells[row, col].Value2.ToString();
            }
            else
            {
                result = "Can not read";
            }
            ClearUp();
            return result;
        }

        public static void ReadExcel(string path, int page) // Methrod to set instance of Excel
        {
            paths = path;
            try
            {
                wb = excel.Workbooks.Open(path);
                ws = wb.Sheets[page];
                rang = ws.UsedRange;
            }
            catch
            {
                CreateNewFile(page);
            }
        }

        public static void CreateNewFile(int page)
        {
            wb = excel.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            ws = wb.Worksheets[1];
            rang = ws.UsedRange;
            for (int i = 1; i < page; i++)
            {
                CreateNewSheet();
            }
            ws = wb.Worksheets[page];
        }

        public static void CreateNewSheet()
        {
            Worksheet aa = wb.Worksheets.Add(After: ws);
        }

        public static void WriteRowColExcel(int row, int col, List<string> Data, int page, string path, bool color) // write a row in multiple column
        {
            paths = path;
            ReadExcel(path, page);
            row++;
            row++;
            col++;
            for (int i = 0; i < Data.Count; i++)
            {
                SetColor(row, col, color);
                rang.Cells[row, col].Value = Data[i];
                col++;
            }
            wb.SaveAs(paths, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                                    false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            ClearUp();
            //wb.Close();
            //excel.Visible = false;
        }

        public static void SetColor(int row, int col, bool color)
        {
            if (color == true)
            {
                rang.Cells[row, col].Font.Color = Color.Black;
            }
            else if (color == false)
            {
                rang.Cells[row, col].Font.Color = Color.Red;
            }
        }

        public static void keydata2exxcel(string path, List<string> Data, List<bool> color, List<string> header)
        {
            excel = StartExcel();
            var alreadyopen = false;
            foreach (Workbook wb in excel.Workbooks)
            {
                if (path.Contains(wb.Name))
                {
                    alreadyopen = true;
                    break;
                }
            }

            if (!alreadyopen) excel.Workbooks.Open(path);

            excel.Visible = true;
            wb = excel.ActiveWorkbook;
            ws = excel.ActiveWorkbook.Sheets[1];
            rang = ws.UsedRange;
            //Range rng = ws.UsedRange;

            for (int i = 1; i <= header.Count; i++)
            {
                ws.Cells[1, i].Value2 = header[i - 1];
            }

            int nextrow = fnNextrow();

            for (int col = 1; col <= Data.Count; col++)
            {
                //ws.Columns[5].ColumnWidth = 20;
                //ws.Rows[10].RowHeight = 20;
                //ws.Cells[1, 1].Font.Size = 30;
                //ws.Cells[1,1].Font.Name = "Arial"; 
                SetColor(nextrow, col, color[col - 1]);
                ws.Cells[nextrow, col].Value2 = Data[col - 1];
            }
            wb.Save();
            ClearUp();
            //Marshal.ReleaseComObject(ws);
        }

        public static int fnNextrow()
        {
            int ntrow = 1;
            //ws.Cells[nextrow, 1].Value2 = "56655";
            var val = Convert.ToString(ws.Cells[ntrow, 1].Value2);
            while (val != null && ntrow < 50)
            {
                ntrow++;
                val = Convert.ToString(ws.Cells[ntrow, 1].Value2);
            }
            return ntrow;
        }

        private static _Excel.Application StartExcel()
        {
            _Excel.Application instance;
            try
            {
                instance = (_Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                instance = new _Excel.Application();
            }
            return instance;
        }

        private static void ClearUp()
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();

                Marshal.FinalReleaseComObject(rang);
                Marshal.FinalReleaseComObject(ws);

                wb.Close(Type.Missing, Type.Missing, Type.Missing);
                Marshal.FinalReleaseComObject(wb);

                excel.Quit();
                Marshal.FinalReleaseComObject(excel);
            }
            catch (Exception ex)
            {

            }
        }

        public static void copyAlltoClipboard(DataGridView dataGridView1)
        {
            //to remove the first blank column from datagridview
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.SelectAll();
            DataObject dataObj = dataGridView1.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        public static void DataGrid2Excel(DataGridView dataGridView1)
        {
            copyAlltoClipboard(dataGridView1);
            Microsoft.Office.Interop.Excel.Application excel;
            Microsoft.Office.Interop.Excel.Workbook wb;
            Microsoft.Office.Interop.Excel.Worksheet ws;
            object misValue = System.Reflection.Missing.Value;
            excel = new _Excel.Application();
            excel.Visible = true;
            wb = excel.Workbooks.Add(misValue);
            ws = (_Excel.Worksheet)wb.Worksheets.get_Item(1);
            for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
            {
                ws.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
            }
            _Excel.Range CR = (_Excel.Range)ws.Cells[2, 1];
            CR.Select();
            ws.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            //ws.SaveAs("Test22.xlsx");
        }

        public static void DataGrid2Excel2(DataGridView dataGridView1)
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            app.Visible = true;
            worksheet = workbook.Sheets["Sheet1"];
            worksheet = workbook.ActiveSheet;
            for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
            {
                worksheet.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
            }
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value != null)
                    {
                        worksheet.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                    }
                    else
                    {
                        worksheet.Cells[i + 2, j + 1] = "";
                    }
                }
            }
        }
    }
}
//ws.Columns[5].ColumnWidth = 20; set width column for excel
//Marshal.ReleaseComObject(ws); clear Runtime Callable Wrapper (RCW) of the COM object.