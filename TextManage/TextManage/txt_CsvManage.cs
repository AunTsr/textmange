﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextManage
{
    public class txt_CsvManage
    {
        public void WriteCsv<T>(List<T> Data)
        {
            using (SaveFileDialog save = new SaveFileDialog() { Filter = "CSV|*.csv", ValidateNames = true})
            {
                if(save.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        using (var sw = new StreamWriter(save.FileName))
                        {
                            var Writer = new CsvWriter(sw);
                            Writer.WriteHeader(typeof(T));

                            foreach (var dt in Data)
                            {
                                Writer.WriteRecords((IEnumerable<T>)dt);
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        public List<T> ReadCsv<T>()
        {
            using (OpenFileDialog Open = new OpenFileDialog() { Filter = "CSV|*.csv", ValidateNames = true })
            {
                if (Open.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        var sr = new StreamReader(new FileStream(Open.FileName, FileMode.Open));
                        var Csv = new CsvReader(sr);
                        return Csv.GetRecords<T>().ToList();
                    }
                    catch
                    {

                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
